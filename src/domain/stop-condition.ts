export enum StopCondition {
  ACCURACY,
  LOOPS_AMOUNT,
  CALC_AMOUNT
}
