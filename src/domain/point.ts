export interface Point {
  y1: number;
  y2: number;
  val: number;
}
