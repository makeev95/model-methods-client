import {CalcMethod} from './calc-method';
import {StopCondition} from './stop-condition';
import {Extreme} from './extreme';

export class CalcRequest {
  step1: number;
  step2: number;
  y1: number;
  y2: number;
  y1Min: number;
  y1Max: number;
  y2Min: number;
  y2Max: number;
  method: CalcMethod;
  conditions: StopCondition [];
  loopsAmount: number;
  accuracy: number;
  calcAmount: number;
  extreme: Extreme;
}
