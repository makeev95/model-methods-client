import {StopCondition} from './stop-condition';
import {Point} from './point';

export class CalcResponse {
  stopCondition: StopCondition;
  y1: number[];
  y2: number[];
  val: number[];
  extremeVal: Point;
}
