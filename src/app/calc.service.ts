import {Injectable} from '@angular/core';
import {CalcRequest} from '../domain/calc-request';
import {HttpClient} from '@angular/common/http';
import {CalcResponse} from "../domain/calc-response";
import {Observable} from "rxjs/Observable";

@Injectable()
export class CalcService {

  constructor(private http: HttpClient) {
  }


  post(req: CalcRequest): Promise<any> {
    return this.http.post('http://82.137.162.137:82/calc', req)
      .toPromise();
  }

}
