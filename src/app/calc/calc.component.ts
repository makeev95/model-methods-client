import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {CalcService} from '../calc.service';
import {CalcRequest} from '../../domain/calc-request';
import {CalcMethod} from '../../domain/calc-method';
import {StopCondition} from '../../domain/stop-condition';
import {Extreme} from '../../domain/extreme';
import {CalcResponse} from '../../domain/calc-response';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.scss']
})
export class CalcComponent implements OnInit {

  @ViewChild('chart2D') chart2D: ElementRef;
  @ViewChild('chart3D') chart3D: ElementRef;

  req: CalcRequest = new CalcRequest();
  resp: CalcResponse = null;
  methods: CalcMethod[] = Object.keys(CalcMethod).filter(e => parseInt(e, 10) >= 0).map(k => CalcMethod[k]);
  extrema: Extreme[] = Object.keys(Extreme).filter(e => parseInt(e, 10) >= 0).map(k => Extreme[k]);

  constructor(private calcService: CalcService) {
  }

  ngOnInit() {
    this.req.step1 = .5;
    this.req.step2 = .5;
    this.req.y1 = -.5;
    this.req.y2 = .5;
    this.req.y1Min = -.5;
    this.req.y1Max = .5;
    this.req.y2Min = -.5;
    this.req.y2Max = .5;
    this.req.conditions = [StopCondition.LOOPS_AMOUNT];
    this.req.loopsAmount = 100;
    this.req.extreme = Extreme.MIN;
  }

  changeConditions(e: any, num: number) {

    let cnd: StopCondition;

    switch (num) {
      case 0:
        cnd = StopCondition.ACCURACY;
        break;
      case 1:
        cnd = StopCondition.LOOPS_AMOUNT;
        break;
      default:
        cnd = StopCondition.CALC_AMOUNT;
    }

    const index = this.req.conditions.indexOf(cnd);
    if (index > -1) {
      this.req.conditions.splice(index, 1);
    } else {
      this.req.conditions.push(cnd);
    }
  }

  hasCondition(num: number) {
    let cnd: StopCondition;

    switch (num) {
      case 0:
        cnd = StopCondition.ACCURACY;
        break;
      case 1:
        cnd = StopCondition.LOOPS_AMOUNT;
        break;
      default:
        cnd = StopCondition.CALC_AMOUNT;
    }

    return this.req.conditions.indexOf(cnd) > -1;
  }

  async post(e: any) {
    try {
      this.resp = await this.calcService.post(this.req);
      if (this.resp.extremeVal !== null) {
        this.resp.extremeVal.y1 = Math.round(this.resp.extremeVal.y1 * 100.) / 100.;
        this.resp.extremeVal.y2 = Math.round(this.resp.extremeVal.y2 * 100.) / 100.;
        this.resp.extremeVal.val = Math.round(this.resp.extremeVal.val * 100.) / 100.;
      }
      this.build2D();
      this.build3D();
    } catch (e) {
      this.resp = null;
      return;
    }
  }

  private async build2D() {
    const element = this.chart2D.nativeElement;
    // noinspection JSSuspiciousNameCombination
    const trace = {
      x: this.resp.y1,
      y: this.resp.y2,
      mode: 'lines+markers',
      name: 'chart'
    };

    const data = [trace];
    const style = {
      margin: {t: 0}
    };
    Plotly.purge(element);
    Plotly.plot(element, data, style);
  }

  private async build3D() {
    const element = this.chart3D.nativeElement;
    // noinspection JSSuspiciousNameCombination
    const data = [
      {
        opacity: 0.8,
        color: 'rgb(300,100,200)',
        type: 'mesh3d',
        x: this.resp.y1,
        y: this.resp.y2,
        z: this.resp.val,
        mode: 'lines+markers'
      }
    ];
    const style = {
      margin: {t: 0}
    };
    Plotly.purge(element);
    Plotly.plot(element, data, style);
  }

}
